﻿using UnityEngine;
using System.Collections;

public class Module : MonoBehaviour {
	public ModuleType type = ModuleType.None;

	public Connector[] GetConnectors()
	{
		return GetComponentsInChildren<Connector> ();
	}
}
