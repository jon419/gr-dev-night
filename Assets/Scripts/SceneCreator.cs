﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SceneCreator : MonoBehaviour {
	public Module[] Modules;

	// Use this for initialization
	void Start ()
	{
		var startModule = (Module)Instantiate (GetRandom (Modules.Where (m => m.type == ModuleType.Room).ToArray()), transform.position, transform.rotation);

		var currentConnectors = new List<Connector> (startModule.GetConnectors ());

		for(int i = 0; i < 5; i++)
		{
			var newConnectors = new List<Connector>();

			foreach(var connector in currentConnectors)
			{
				if(Random.value > 0.8f) continue;

				var nextModuleType = GetRandom(connector.connectsTo);

				var nextModulePrefab = GetRandom(Modules.Where(m => m.type == nextModuleType).ToArray());
				var nextModule = (Module)Instantiate(nextModulePrefab);

				var nextModuleConnectors = nextModule.GetConnectors();
				var connectorToMatch = GetRandom(nextModuleConnectors);

				RotateToMatch(connector, connectorToMatch);

				newConnectors.AddRange(nextModuleConnectors.Where(c => c != connectorToMatch));
			}

			currentConnectors = newConnectors;
		}
	}

	private static T GetRandom<T>(T[] array)
	{
		return array[Random.Range(0, array.Length)];
	}

	private void RotateToMatch(Connector oldConnector, Connector newConnector)
	{
		var newModule = newConnector.transform.parent;
		var rotationCorrection = VectorAngle (-oldConnector.transform.forward) - VectorAngle (newConnector.transform.forward);

		newModule.RotateAround (newModule.transform.position, Vector3.up, rotationCorrection);

		var translationCorrection = oldConnector.transform.position - newConnector.transform.position;
		newModule.transform.position += translationCorrection;
	}

	private static float VectorAngle(Vector3 vector)
	{
		return Vector3.Angle (Vector3.forward, vector) * Mathf.Sign (vector.x);
	}
}
